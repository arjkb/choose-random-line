# Program to choose a random line from a file.
#
# Arjun Krishna Babu | 28 August 2022

#!/usr/bin/python

import random
import sys

def getLineListFromFile(filename: str) -> list[str]:
    with open(filename, 'r') as file:
        lines = [line.strip() for line in file.readlines()]
    return lines

def main():
    if len(sys.argv) < 2:
        print("choose_random_line.py: missing file operand")
        print("Usage: choose_random_line.py PATH/TO/INPUT_FILE")
        sys.exit(1)

    try:
        filename = sys.argv[1]
        print(random.choice(getLineListFromFile(filename)))
    except FileNotFoundError as err:
        print(f"Exception: {err}")



if __name__ == '__main__':
    main()
